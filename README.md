
Here's the proper way to directly query to database in MAGENTO 2

e.g.
<pre>
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
    $connection = $resource->getConnection();
    $select = $connection->select()
        ->from('tm_brand','logo')
        ->where('brand_id =?', $_product->getData('brand_id'));
    $brand_file = $connection->fetchOne($select);
</pre>